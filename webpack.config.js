const path = require("path")
const htmlPlugin = require("html-webpack-plugin")
const copyPlugin = require("copy-webpack-plugin")

module.exports = {
    mode: "development",
    entry: "./src/app.js",
    output: {
        filename: "bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true
    },
    module: {
        rules: [
            {test: /\.css$/, use: ['style-loader', 'css-loader']},
            {test: /\.(ttf|png|jpg)$/, type: 'asset'}
        ]
    },
    devServer: {
        contentBase: "./dist"
    },
    plugins: [
        new htmlPlugin({template: "./src/html/index.html"}),
        new copyPlugin({
            patterns: [
                {context: 'src/html', from: '**/*.(jpg|png)'}
            ]
        })
    ]
}